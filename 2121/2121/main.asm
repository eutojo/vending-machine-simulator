.include "m2560def.inc"

.def row = r16              ; current row number
.def col = r17              ; current column number
.def rmask = r18            ; mask for current row during scan
.def cmask = r19            ; mask for current column during scan
.def temp1 = r20 
.def temp2 = r21
.def temp = r22
.def lcd = r23
.def keyPressed = r27
.def debounceFlag = r30		
.def currentScreen = r31		

.equ PORTLDIR = 0xF0        
.equ INITCOLMASK = 0xEF   
.equ INITROWMASK = 0x01   
.equ ROWMASK = 0x0F     

.equ startUp = 1
.equ select = 2
.equ empty = 3
.equ coin = 4
.equ delivery = 5
.equ adminMode = 6

;===== LCD MACROS =====
; do a command
.macro do_lcd_command
	ldi lcd, @0
	call lcd_command
	call lcd_wait
.endmacro

; display a letter/word
.macro do_lcd_data
	ldi lcd, @0
	call lcd_data
	call lcd_wait
.endmacro

; display data from data space
.macro do_lcd_bdata
	push temp
	lds temp, @0
	mov lcd, temp
	subi lcd, -'0'
	call lcd_data
	call lcd_wait
	pop temp
.endmacro

; display data from register
.macro do_lcd_rdata
	push lcd
	mov lcd, @0
	subi lcd, -'0'
	call lcd_data
	call lcd_wait
	pop lcd
.endmacro

; display digits from data space
.macro do_lcd_bdigits
	clr keyPressed
	loadByte toConvert, @0			
	call convertDigits	
.endmacro

; just to clear the cursor
.macro	clear_lcd_cursor
	do_lcd_command 0b00001100
.endmacro

;===== INSTRUCTIONAL MACROS =====
; clears the 2 bytes associated w memory
.macro clear
    ldi YL, low(@0)     
    ldi YH, high(@0)
    clr temp 
    st Y+, temp         
    st Y, temp
.endmacro

; allows for greater branch jumps
.macro equalTo
	brne pc+2
	rjmp @0
.endmacro

; allows for greater branch jumps
.macro notEqual
	breq pc+2
	rjmp @0
.endmacro

; allows for greater branch jumps
.macro greaterThan
	brlt pc+2
	rjmp @0
.endmacro

;===== DATA SPACE OPERATIONS ====
; decreasing to a minimum of 0
.macro decrease
	lds temp, @0
	ldi temp1, 0
	cpse temp, temp1
	dec temp
	sts @0, temp
.endmacro

; increasing to no maximum (general increase)
.macro increase
	push temp
	lds temp, @0
	inc temp
	sts @0, temp
	pop temp
.endmacro

; increasing the cost to a maximum of 3
.macro increaseCost
	push temp
	push temp1
	lds temp, @0
	ldi temp1, 3
	cpse temp, temp1
	inc temp
	sts @0, temp
	pop temp1
	pop temp
.endmacro

; decreasing the cost to a minimum of 1
.macro decreaseCost
	push temp
	push temp1
	lds temp, @0
	ldi temp1, 1
	cpse temp, temp1
	dec temp
	sts @0, temp
	pop temp1
	pop temp
.endmacro

; increasing the stock to a maximum of 255
.macro increaseItem
	push temp
	push temp1
	lds temp, @0
	ldi temp1, 255
	cpse temp, temp1
	inc temp
	sts @0, temp
	pop temp1
	pop temp
.endmacro

; similar to mov for registers
.macro loadByte
	push temp
	lds temp, @1
	sts @0, temp
	pop temp
.endmacro

; similar to ldi for registers
.macro loadImByte
	push temp
	ldi temp, @1
	sts @0, temp
	pop temp
.endmacro

.dseg
; counters for the timers
debounceCounter:		
    .byte 2             
secondCounter:
	.byte 2
fiveSecondCounter:
	.byte 2
halfSecondCounter:
	.byte 2				
motorCounter:
	.byte 2
; raised when the * button is pressed on select screen
adminFlag:
	.byte 2
; holds the number pressed
number:
	.byte 2
; holds the number of coins required
remaining:
	.byte 2
; raised when a refund is to occur
abortFlag:
	.byte 2
; raised when the motor is on
motorFlag:
	.byte 2
; counts the number of coins inserted on the coins screen
insertedCoins:
	.byte 2
; raised when changing screens
screenFlag:
	.byte 2
; holds the item requested on select screen
requestedItem:
	.byte 2
; raised when the LED lights are on
ledFlag:
	.byte 2
; counts to 3
ledCount:
	.byte 2
; debouncing flags for the PB buttons
pb0flag:
	.byte 2
pb1flag:
	.byte 2
; counter for the stage the potentiometer is at with regards
; to putting in a coin
decreaseCoinFlag:
	.byte 2
; holds the number pressed to display on the admin screen. its
; def
adminScreenCount:
	.byte 2
; used to retrieve and show cost/stock
costDisplay:
	.byte 2
stockDisplay:
	.byte 2
; used to display digits greater than 9
toConvert:
	.byte 2
hundredsDigit:
	.byte 2
tensDigit:
	.byte 2
; costs of the different items
cost1:
	.byte 2
cost2:
	.byte 2
cost3:
	.byte 2
cost4:
	.byte 2
cost5:
	.byte 2
cost6:
	.byte 2
cost7:
	.byte 2
cost8:
	.byte 2
cost9:
	.byte 2
; stock of the different items
item1:
	.byte 2
item2:
	.byte 2
item3:
	.byte 2
item4:
	.byte 2
item5:
	.byte 2
item6:
	.byte 2
item7:
	.byte 2
item8:
	.byte 2
item9:
	.byte 2
                        

; initialising interrup vectors
.cseg
.org 0x0000
    jmp RESET    
.org OVF0addr
    jmp Timer0OVF
.org OVF2addr
	jmp Timer2Ovf    
.org INT0addr
	jmp EXT_INT0
.org INT1addr 
	jmp EXT_INT1
.org ADCCaddr
	jmp Potentiometer
.org OVF3addr
	jmp Timer3Ovf

RESET:
	; initialising the stack
	ldi temp, low(RAMEND)  
    out SPL, temp
    ldi temp, high(RAMEND)
    out SPH, temp

	; initialising the keypad
    ldi temp1, PORTLDIR     
    sts DDRL, temp1         
	
	; initialise LCD display
	ser temp
	out DDRF, temp
	out DDRA, temp
	out DDRE, temp

	clr temp
	out PORTF, temp1

	; set up the LCD
	do_lcd_command 0b00111000 
	call sleep_5ms
	do_lcd_command 0b00111000 
	call sleep_1ms
	do_lcd_command 0b00111000 
	do_lcd_command 0b00111000 
	do_lcd_command 0b00001000 
	do_lcd_command 0b00000001 
	do_lcd_command 0b00000110 
	do_lcd_command 0b00001110

	do_lcd_command 0b11000000

	; initialise LED
	ser temp
	out DDRG, temp
	out DDRC, temp

	clr temp
	out PORTC, temp
	out PORTG, temp

	; initialise motor
	ser temp
    sts DDRH, temp	
	
	; initialising the potentiometer
	ldi temp1, (3<<REFS0) | (0<<ADLAR) | (0<<MUX0)
	sts ADMUX, temp1
	ldi temp1, (1<<MUX5) | (0<<ADSC) | (0<<ADIE)
	sts ADCSRB, temp1
	ldi temp1, (1<<ADEN) | (1<<ADSC) | (1<<ADIE) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0)
	sts ADCSRA, temp1

	; initialise push buttons
	ldi temp, (2 << ISC00)      
    sts EICRA, temp            
    in temp, EIMSK              
    ori temp, (1<<INT0)
    out EIMSK, temp

	ldi temp, (2 << ISC10)      
    sts EICRA, temp            
    in temp, EIMSK             
    ori temp, (1<<INT1)
    out EIMSK, temp

	;initialise timers
	ldi temp, 0b00000000
    out TCCR0A, temp
    ldi temp, 0b00000010
    out TCCR0B, temp        
    ldi temp, 1<<TOIE0      
    sts TIMSK0, temp        

	ldi temp, 0b00000000
	sts TCCR2A, temp
	ldi temp, 1 << TOIE2
	sts TIMSK2, temp
	ldi temp, 0b00000010
	sts TCCR2B, temp

	ldi temp, 0b00000000
	sts TCCR3A, temp
	ldi temp, 1 << TOIE3
	sts TIMSK3, temp
	ldi temp, 0b00000010
	sts TCCR3B, temp

	sei						; enable global interrupt

    rjmp main        

.equ LCD_RS = 7
.equ LCD_E = 6
.equ LCD_RW = 5
.equ LCD_BE = 4

.macro lcd_set
	sbi PORTA, @0
.endmacro
.macro lcd_clr
	cbi PORTA, @0
.endmacro


;===== TIMER0 =====
; used for debouncing keys and buttons
Timer0OVF:
	in temp, SREG
    push temp       
    push YH         
    push YL
	push r27
	push r26

	checkDebounceFlags:				
		cpi debounceFlag, 1				; if either keys or buttons are debouncing, start the deboucing
		breq startDebouncing		
		lds temp, pb0flag
		cpi temp, 1
		breq startDebouncing
		lds temp, pb1flag
		cpi temp, 1
		breq startDebouncing
		rjmp timer0Epilogue					; if none are debouncing, no need to start				

	startDebouncing:
		lds r26, debounceCounter
    	lds r27, debounceCounter+1
    	adiw r27:r26, 1						; increase the debounce counter

    	cpi r26, low(128)					; compare it to see if enough time has passed
    	ldi temp, high(128)    
    	cpc temp, r27
    	brne notTime						; if not, branch
		clr debounceFlag 					; if it's time, clear the debounce counter as well as the debounce flags
	   	clear debounceCounter	
		clear pb1flag
		clear pb0flag
		rjmp timer0Epilogue

	notTime: 		
		sts debounceCounter, r26				; if it's not time, just store the new counter values
		sts debounceCounter+1, r27
	    
timer0Epilogue:
    pop r26         
    pop r27         
    pop YL
    pop YH
    pop temp
    out SREG, temp
    reti            

;===== TIMER2 =====
; used for changing the screens back to the start screen after three seconds,
; as well as flashing the LEDs during the empty and delivery screens.
; also responsible for couting the five seconds required to change to admin mode 
; when the star key is pressed
Timer2Ovf:
	push temp
	in temp, SREG
	push temp1
	push r26
	push r27
	push YH
	push YL

	cpi currentScreen, select		; if it's in the select screen, no need to start the 3 second countdown
	equalTo adminFlagCheck			; instead, check to see if the * button has been pressed
	clear fiveSecondCounter			; if not, clear the five second counter
	lds temp, screenFlag			; check to see if the LCD has recently changed screens
	cpi temp, 1
	breq startTimer2				; if it has, start counting down from 3
	clear secondCounter
	rjmp timer2Epilogue				; if not, don't start

	startTimer2:
		lds r26, secondCounter		
		lds r27, secondCounter+1
		adiw r27:r26, 1				; add 1 to the counter
	
		cpi r26, low(23600)			; check to see if enough time has elapsed
		ldi temp, high(23600)
		cpc r27, temp
		brne notThreeSecond			; if not, branch out

	threeSecond:
		clear secondCounter			; if three seconds has elapsed, clear the second counter
		clear screenFlag			; and clear the screen flag to signal it to change screens accordingly
		cpi currentScreen, delivery
		NotEqual timer2Epilogue
		rjmp timer2Epilogue

	notThreeSecond:
		sts secondCounter, r26		; if not three seconds, store the counter
		sts secondCounter+1, r27
	
		cpi currentScreen, empty	; check to see if it's on the empty screen
		breq startEmptyFlash		; if it is, the LED's must be flashing
		cpi currentScreen, delivery	; also check to see if it's on the delivery screen
		breq startEmptyFlash		; as that also needs to have LEDs flashing

		rjmp timer2Epilogue			; if neither, don't flash

	startEmptyFlash:
		lds r26, halfSecondCounter		
		lds r27, halfSecondCounter+1
		adiw r27:r26, 1				; add one to the half second counter

		cpi r26, low(3905)			; check to see if half a second has ela[sed
		ldi temp, high(3905)
		cpc r27, temp	
		NotEqual notHalfSecond		; if not, branch out

	halfSecond:
		clear halfSecondCounter		; clear the one second counter
		lds temp, ledFlag			; if yes, check to see if the LED is on
		cpi temp, 1					; if it's on, turn it off and vice versa
		breq turnOff
	turnOn:	
		call turnOnLED				; call the function to turn on the LEDs
		rjmp timer2Epilogue
	turnOff:
		call turnOffLED				; call the function to turn off the LEDs
		rjmp timer2Epilogue
		
	notHalfSecond:
		sts halfSecondCounter, r26	; store new values
		sts halfSecondCounter+1, r27
		rjmp timer2Epilogue

	adminFlagCheck:
		lds temp, adminFlag
		cpi temp, 1				; check to see if the * key has been pressed
		brne timer2Epilogue
		lds r26, fiveSecondCounter		
		lds r27, fiveSecondCounter+1
		adiw r27:r26, 1

		cpi r26, low(39050)				; check to see if 5 seconds has passed
		ldi temp, high(39050)
		cpc r27, temp
		brne notFiveSecond				; if not, branch

	fiveSecond:
		clear fiveSecondCounter			; if yes, clear the five second counter
		clr keyPressed					; clear the key pressed flag
		ldi temp, -1					; raise the screen flag to go into admin mode
		sts screenFlag, temp
		rjmp timer2Epilogue

	notFiveSecond:
		sts fiveSecondCounter, r26		; store the new values if not five seconds
		sts fiveSecondCounter+1, r27
		rjmp timer2Epilogue

	timer2Epilogue:
		pop YL
		pop YH
		pop r27
		pop r26
		pop temp1
		out SREG, temp
		pop temp
		reti

;===== INT0 INTERRUPT =====
; instructions for the right push button (pb0)
; used in admin mode to decrease the number of items
; also used on the empty screen to skip the flashing
EXT_INT0:
	in temp, SREG
	push YH
	push YL
	push temp
	push temp1

	lds temp, pb0flag					; check if the button is debouncing
	cpi temp, 0							; if it is, don't do anything
	NotEqual EXT_INT0_Epilogue

	loadImByte pb0flag, 1				; raise the pb0 debounce flag
	
	cpi currentScreen, empty			; check to see if the current screen is empty
	brne notEmptyScreen0			
	clear screenFlag					; if it is, clear the screen flag to change the LCD display

	notEmptyScreen0:
		cpi currentScreen, adminMode	; if it isnt check to see if it's in admin mode
		notEqual EXT_INT0_Epilogue		; if it's not, exit
		lds temp, adminScreenCount		; if it is, check to see which admin screen it's currently on
		cpi temp, 9		
		equalTo max9
		cpi temp, 8
		equalTo max8
		cpi temp, 7
		equalTo max7
		cpi temp, 6
		equalTo max6
		cpi temp, 5
		equalTo max5
		cpi temp, 4
		equalTo max4
		cpi temp, 3
		equalTo max3
		cpi temp, 2
		equalTo max2

		max1:						; once found, increase the number of items associated with that number
			increaseItem item1		; the macro increases the number of items up to a maximum of 255
			rjmp endMax
		max2:
			increaseItem item2
			rjmp endMax
		max3:
			increaseItem item3
			rjmp endMax
		max4:
			increaseItem item4
			rjmp endMax
		max5:
			increaseItem item5
			rjmp endMax
		max6:
			increaseItem item6
			rjmp endMax
		max7:
			increaseItem item7
			rjmp endMax
		max8:
			increaseItem item8
			rjmp endMax
		max9:
			increaseItem item9
		endMax:
			loadImByte screenFlag, -1		; raise the screen flag to refresh the admin screen
			
	EXT_INT0_Epilogue:
		pop temp1
		pop temp
		pop YL
		pop YH
		out SREG, temp
		reti

; ===== INT1 INTERRUPT =====
; instructions for the right push button (pb1)
; used in admin mode to decrease the number of items
; also used on the empty screen to skip the flashing	
EXT_INT1:
	in temp, SREG
	push YH
	push YL
	push temp
	push temp1

	lds temp, pb1flag						; check if the button is debouncing	
	cpi temp, 0								; if it is, don't do anything
	NotEqual EXT_INT1_Epilogue				

	loadImByte pb1flag, 1					; raise the pb1 debounce flag

	cpi currentScreen, empty				; check to see if the current screen is empty
	NotEqual notEmptyScreen1				
	clear screenFlag						; if it is, clear the screen flag to change the LCD display

	notEmptyScreen1:
		cpi currentScreen, adminMode		; if it isn't check to see if it's in admin mode
		NotEqual EXT_INT1_Epilogue			; if it's not, exit
		lds temp, adminScreenCount			; if it is, check to see which admin screen it's currently on
		cpi temp, 9
		EqualTo min9
		cpi temp, 8
		EqualTo min8
		cpi temp, 7
		EqualTo min7
		cpi temp, 7
		EqualTo min5
		cpi temp, 6
		EqualTo min4
		cpi temp, 5
		EqualTo min3
		cpi temp, 4
		EqualTo min2
		cpi temp, 3
		EqualTo max3
		cpi temp, 2
		EqualTo max2

		min1:					; once found, decrease the number of items associated with that number
			decrease item1		; the macro decreases the number of items to a minimum of 0
			rjmp endMin
		min2:
			decrease item2
			rjmp endMin
		min3:
			decrease item3
			rjmp endMin
		min4:
			decrease item4
			rjmp endMin
		min5:
			decrease item5
			rjmp endMin
		min6:
			decrease item6
			rjmp endMin
		min7:
			decrease item7
			rjmp endMin
		min8:
			decrease item8
			rjmp endMin
		min9:
			decrease item9
		endMin:
			loadImByte screenFlag, -1		; raise the screen flag to refresh the admin screen

	EXT_INT1_Epilogue:
		pop temp1
		pop temp
		pop YL
		pop YH
		out SREG, temp
		reti

;===== TIMER3 =====
; used to turn on/off the motor when returning inserted coins.
; it is independent of the screen changes and will keep going
; until all the coins have been returned.
Timer3OVF:
	push temp
	in temp, SREG
	push YH
	push YL
	push r27
	push r26
	push temp1

	lds temp, abortFlag					; check to see if the abort flag has been raised
	cpi temp, 1							; that is, if there are coins to be returned
	brne timer3Epilogue					; if none, exit

	refund:
		lds temp, insertedCoins			; get the number of inserted coins

		lds r26, motorCounter			; add 1 to the motor counter
		lds r27, motorCounter+1
		adiw r27:r26, 1

		cpi r26, low(5)					; check to see if a quarter of a second has passed
		ldi temp, high(5)
		cpc r27, temp
		NotEqual notQuarter				; if not, branch

		quarter:
			lds temp, motorFlag				; if yes, check to see if the motor is on
			cpi temp, 1						; if the motor is on, turn it off
			breq shutMotor					; if it's off, turn it on
		startMotor:
			rcall turnOnMotor
			rjmp quarterCont
		shutMotor:
			rcall turnOffMotor
			decrease insertedCoins
			lds temp, insertedCoins
			cpi temp, 0
			brne quarterCont
			clear abortFlag
		quarterCont:
			clear motorCounter				; clear the motor counter to signify the end of the
			rjmp timer3Epilogue				; quarter of a second

		notQuarter:
			sts motorCounter, r26			; if it's not a quarter of a second
			sts motorCounter+1, r27			;store the value

	timer3Epilogue:
		
		pop temp1
		pop r26
		pop r27
		pop YL
		pop YH
		out SREG, temp
		pop temp
		reti

;===== POTENTIOMETER =====
; the potentiometer is used when inserting coins
; coins are only inserted if the potentiometer goes from low, high, low
; consecutively.
; the coinFlags are used as follows:
; 1 - only increased to 1 if it starts at a fully anticlockwise position (this is position 3)
; 2 - only increased if it is turned from a fully clockwise position (this is position 2)
;	  to a fully clockwise position
; after that, a coin insert will be detected if it is then turned fully anticlockwise
Potentiometer:
	push temp
	in temp, SREG
	push r26
	push r27
	push YH
	push YL

	cpi currentScreen, coin						; check to see if it's on the coin screen page
	breq readPot								; if yes, get the pot reading
	clear decreaseCoinFlag						; if not, clear the count and
	rjmp PotentiometerEnd						; exit

	readPot:
		rcall turnOnPot							; turn on the pot
		lds r26, ADCL							; load the pot reading valused into the registers
		lds r27, ADCH							; for comparison

		cpi r26, low(0x3FF)						; check to see if it is turned fully clockwise
		ldi temp, high(0x3FF)
		cpc r27, temp
		breq maxReading							; if it is, it is a max reading
		cpi r26, low(0)

		ldi temp, high(0)						; check to see if it is turned fully anti- clockwise
		cpc r27, temp
		breq minReading							; if it is, it is a min reading

		rjmp PotentiometerEnd					; if neither, just end

		minReading:
			lds temp, decreaseCoinFlag				; check to see if it is the first time it is at an anticlockwise
			cpi temp, 0								; position
			brne checkTwo							; if not, check to see if it is the second
			rcall turnOffPot						; turn off the potentiometer as the reading has been complete
			increase decreaseCoinFlag				; if it is the first, increase the coin flag to 1
			rjmp PotentiometerEnd

		checkTwo:
			lds temp, decreaseCoinFlag				; check to see if it is the second time
			cpi temp, 2								; if it's not, then it means that it has remained in this position
			brne PotentiometerEnd					; so do nothing
			increase insertedCoins					; if it is, then a coin insert is detected
			decrease remaining						; decrease the amount of coins remaining to be inserted
			loadImByte screenFlag, -2				; raise the screen flag to refresh the coins screen
			clear decreaseCoinFlag					; start the coin flag from 0
			rcall turnOffPot						; turn off the potentiometer as the reading has been complete
			rjmp PotentiometerEnd

		maxReading:
			lds temp, decreaseCoinFlag				; if it is a max reading, check to see if it has previously been at minimum
			cpi temp, 1								; if not, then do nothing as it has to start from minimum
			brne PotentiometerEnd
			increase decreaseCoinFlag				; if it has, then increase the coin flag
			rcall turnOffPot						; turn off the potentiometer as the reading has been complete

	PotentiometerEnd:
		ldi temp1, (1<<ADEN) | (1<<ADSC) | (1<<ADIE) | (1<<ADPS2) | (1<<ADPS1) | (1<<ADPS0) ; reload this because apparently
		sts ADCSRA, temp1																	; it is changed in the process
		pop YL
		pop YH
		pop r27
		pop r26
		out SREG, temp
		pop temp
		reti

;==== MAIN =====
; the main function which initialises the stock and costs
main:
	
	; clear the bytes of memory used so that they start at 0
	clear debounceCounter       
	clear ledFlag
	clear adminFlag
	clear adminScreenCount
	clear secondCounter
	clear fiveSecondCounter
	clear insertedCoins
	clear decreaseCoinFlag
	clear remaining
	clear abortFlag
	clear motorCounter

	; stock the items based on their corresponding numbers
	loadImByte item1, 1
	loadImByte item2, 2
	loadImByte item3, 3
	loadImByte item4, 4
	loadImByte item5, 5
	loadImByte item6, 6
	loadImByte item7, 7
	loadImByte item8, 8
	loadImByte item9, 9

	; load the costs based on whether theyre odd or even
	loadImByte cost1, 1
	loadImByte cost2, 2
	loadImByte cost3, 1
	loadImByte cost4, 2
	loadImByte cost5, 1
	loadImByte cost6, 2
	loadImByte cost7, 1
	loadImByte cost8, 2
	loadImByte cost9, 1

	rcall turnOffPot		; turn off the pot as it is not used		

	rjmp startUpScreen		; immediately go to this screen upon startup


getKey:
	clr keyPressed				; indicates that no button has been pressed
	clear adminFlag				; as it is reset if the keypad must be rescanned

initKeypad:
    ldi cmask, INITCOLMASK		; initial column mask
    clr col						; initial column
	clr temp
	clr temp1
	clr temp2

	cpi debounceFlag, 1			; check if the button is still debouncing
	breq initKeypad				; ignore the press if it is

	ldi debounceFlag, 1			; if not, raise the debounce flag

	lds temp, screenFlag		; check the screen flags to see what to do
	cpi temp, 0					; if the screen flag is 0, the lcd display must
	breq changeScreen			; be changed
	rjmp continue				; if not, continue scanning for key presses

changeScreen:
	cpi currentScreen, startUp	; after 3 seconds on the start up screen, the empty
	EqualTo selectScreen		; screen or the delivery screen, change the LCD
	cpi currentScreen, empty	; display back to the main menu (select screen)
	EqualTo selectScreen		
	cpi currentScreen, delivery
	EqualTo selectScreen

continue:
	lds temp, screenFlag		; if the screen flag has been changed
	cpi temp, -1				; to -1, then the LCD must display the
	EqualTo adminScreen			; admin screen, either to refresh or for the first time,
	cpi temp, -2				; to -2, then the LCD must display the coins screen
	EqualTo coinsScreen			; either to refresh or for the first time

colloop:
    cpi col, 4					; check if all the keys have been scanned
    breq getKey					; if it has, that means no buttons have been pressed
    sts PORTL, cmask			; if it hasn't, scan a column

    ldi temp1, 0xFF				; slow down the scan operation.

delay:
    dec temp1
    brne delay					; until temp1 is zero? - delay

    lds temp1, PINL				; Read PORTL
    andi temp1, ROWMASK			; Get the keypad output value
    cpi temp1, 0xF				; Check if any row is low
    breq nextcol				; if not - switch to next column

								; If yes, find which row is low
    ldi rmask, INITROWMASK		; initialize for row check
    clr row

rowloop:
    cpi row, 4					; is row already 4?
    breq nextcol				; the row scan is over - next column
    mov temp2, temp1
    and temp2, rmask			; check un-masked bit
    breq checkPress				; if bit is clear, the key is pressed
    inc row						; else move to the next row
    lsl rmask
    rjmp rowloop
    
nextcol:						; if row scan is over
    lsl cmask
    inc col						; increase col value
    rjmp colloop				; go to the next column

checkPress:
	cpi keyPressed, 1			; if this register still holds 1, it means that
	EqualTo initKeypad			; the key is still being pressed from previous

	cpi currentScreen, startUp	; check to see what happens depending on which screen
	equalTo selectScreen		; the LCD is currently displaying
	cpi currentScreen, select
	equalTo selectOptions
	cpi currentScreen, empty
	equalTo emptyOptions
	cpi currentScreen, coin
	equalTo coinsOptions
	cpi currentScreen, adminMode
	equalTo adminOptions
	cpi currentScreen, delivery
	equalTo deliveryOptions
	rjmp selectScreen

;===== SELECT OPTIONS =====
; instructs the board what to do when a key is prerssed
; while on the select screen
selectOptions:
	cpi col, 3					; if a button on column 3 (the letters)
	equalTo initKeypad			; is pressed, do nothing
	cpi row, 3					; if it is not row 3 (and not in column 3)
	NotEqual getNumber			; it is a number
	cpi col, 0					; if it is not row 3 and in column 0 (the #)
	NotEqual initKeypad			; do nothing
	ldi temp, 1					; this could only mean the * is being pressed
	sts adminFlag, temp			; raise the admin flag,
	ldi keyPressed, 1			; as well as the key press flag
	rjmp initKeypad

	getNumber:
		ldi temp, 3				; get the value of the key pressed
		mul temp, row			; number = 3*row + (col +1)
		mov temp, r0
		add temp, col
		inc temp
		
		sts number, temp				
		loadByte requestedItem, number	; load this number into requestedItem

		rcall getCost					; get the cost of the item,
		rcall getStock					; as well as the stock

		loadByte remaining, costDisplay	; load the cost into the amount remaining

		lds temp, stockDisplay

	endCheck:
		cpi temp, 0							; if stock is not zero, display the item					
		NotEqual coinsScreen				; on the coins screen.
		clear remaining						; if it is zero, clear the remaining
		rjmp emptyScreen					; and display the empty screen

;===== EMPTY OPTIONS =====
; doesn't do anything but flash the LEDs,
; but it will react when PB1 or PB0 is pressed.
emptyOptions:
	jmp initKeypad

;===== COINS OPTIONS =====
; the coins screen can be aborted by pressing the # key and
; will cause a refund to occur, depending whether
; coins have already been inserted
coinsOptions:
	cpi col, 2				; if the button pressed is in column 2
	breq checkAbort			; check if it is the # key

	notAbort:
		rjmp initKeypad			; if not, just keep scanning for keys

	checkAbort:
		cpi row, 3				; if it is the # key, check to see the way
		breq abortCoin			; the coins screen will abort
		rjmp notAbort

	abortCoin:
		lds temp, insertedCoins	; get the number of inserted coins
		cpi temp, 0				; if it's zero, do nothing and just
		EqualTo selectScreen		; change to the select screen

	returnCoins:
		rcall turnOnMotor		; if coins have been inserted, turn on the motor
		loadImByte abortFlag, 1	; raise the abort flag to start the refund
		rjmp selectScreen		; and then go back to select screen

;===== DELIVERY OPTIONS =====
; the delivery screen accepts no input
deliveryOptions:
	rjmp initKeypad

;===== ADMIN OPTIONS =====
; the admin screen takes input from both the keypad and the push buttons
; it allows the modifying of item stock and costs
adminOptions:
	cpi col, 3
	breq adminLetters
	cpi row, 3
	EqualTo adminSymbols
	
	ldi temp, 3
	mul temp, row
	mov temp, r0
	add temp, col
	inc temp

	sts adminScreenCount, temp
	rjmp adminScreen
	
	adminLetters:
		cpi row, 2
		equalTo letterC
		cpi row, 1
		equalTo letterB

		letterA:
			lds temp, adminScreenCount		; get the number to be increased
			cpi temp, 9
			equalTo incCost9
			cpi temp, 8
			equalTo incCost8
			cpi temp, 7
			equalTo incCost7
			cpi temp, 6
			equalTo incCost6
			cpi temp, 5
			equalTo incCost5
			cpi temp, 4
			equalTo incCost4
			cpi temp, 3
			equalTo incCost3
			cpi temp, 2
			equalTo incCost2

			incCost1:
				increaseCost cost1
				rjmp adminScreen
			incCost2:
				increaseCost cost2
				rjmp adminScreen
			incCost3:
				increaseCost cost3
				rjmp adminScreen
			incCost4:
				increaseCost cost4
				rjmp adminScreen
			incCost5:
				increaseCost cost5
				rjmp adminScreen
			incCost6:
				increaseCost cost6
				rjmp adminScreen
			incCost7:
				increaseCost cost7
				rjmp adminScreen
			incCost8:
				increaseCost cost8
				rjmp adminScreen
			incCost9:
				increaseCost cost9
				rjmp adminScreen

		letterB:
			lds temp, adminScreenCount		; get the number to be decreased
			cpi temp, 9
			equalTo decCost9
			cpi temp, 8
			equalTo decCost8
			cpi temp, 7
			equalTo decCost7
			cpi temp, 6
			equalTo decCost6
			cpi temp, 5
			equalTo decCost5
			cpi temp, 4
			equalTo decCost4
			cpi temp, 3
			equalTo decCost3
			cpi temp, 2
			equalTo decCost2
		decCost1:
			decreaseCost cost1
			loadByte number, cost1
			rjmp adminScreen
		decCost2:
			decreaseCost cost2
			loadByte number, cost2
			rjmp adminScreen
		decCost3:
			decreaseCost cost3
			loadByte number, cost3
			rjmp adminScreen
		decCost4:
			decreaseCost cost4
			loadByte number, cost4
			rjmp adminScreen
		decCost5:
			decreaseCost cost5
			loadByte number, cost5
			rjmp adminScreen
		decCost6:
			decreaseCost cost6
			loadByte number, cost6
			rjmp adminScreen
		decCost7:
			decreaseCost cost7
			loadByte number, cost7
			rjmp adminScreen
		decCost8:
			decreaseCost cost8
			loadByte number, cost8
			rjmp adminScreen
		decCost9:
			decreaseCost cost9
			loadByte number, cost9
			rjmp adminScreen


		letterC:
			lds temp, adminScreenCount		; get the number to be cleared
			cpi temp, 9
			EqualTo dec9
			cpi temp, 8
			EqualTo dec8
			cpi temp, 7
			EqualTo dec7
			cpi temp, 6
			EqualTo dec6
			cpi temp, 5
			EqualTo dec5
			cpi temp, 4
			EqualTo dec4
			cpi temp, 3
			EqualTo dec3
			cpi temp, 2
			EqualTo dec2

			dec1:
				clear item1
				rjmp adminScreen
			dec2:
				clear item2
				rjmp adminScreen
			dec3:
				clear item3
				rjmp adminScreen
			dec4:
				clear item4
				rjmp adminScreen
			dec5:
				clear item5
				rjmp adminScreen
			dec6:
				clear item6
				rjmp adminScreen
			dec7:
				clear item7
				rjmp adminScreen
			dec8:
				clear item8
				rjmp adminScreen
			dec9:
				clear item9
				rjmp adminScreen

	adminSymbols:
		cpi col, 2					; abort the screen if it is the # key
		EqualTo selectScreen
		rjmp initKeypad

;===== SELECT SCREEN =====
; the select screen is the main screen and just displays "SELECT SCREEN"
selectScreen:
	ldi currentScreen, select			; set the current screen to "select"
	rcall turnOffLED					; all LEDs should be off
	rcall turnOffMotor					; the motor should also be off

	loadImByte adminScreenCount, 1		; reset the admin screen to display 1 by default
	loadImByte screenFlag, 1			; raise the screen flag
	
	do_lcd_command 0b00000001			; clear the screen

	do_lcd_data 'S'
	do_lcd_data 'e'
	do_lcd_data 'l'
	do_lcd_data 'e'
	do_lcd_data 'c'
	do_lcd_data 't'
	do_lcd_data ' '
	do_lcd_data 'I'
	do_lcd_data 't'
	do_lcd_data 'e'
	do_lcd_data 'm'

	clear_lcd_cursor					; clear the cursor

	ldi keyPressed, 1					; indicate that a key has been pressed
	rjmp initKeypad

;===== EMPTY SCREEN =====
; the empty screen shows "Out of Stock" as well as the number requested
emptyScreen:
	ldi currentScreen, empty			; set the current screen to "empty"			
	rcall turnOnLED						; turn on the LED to start the flash

	loadImByte screenFlag, 1			; raise the screen flag

	do_lcd_command 0b00000001			; clear the lcd

	do_lcd_data 'O'
	do_lcd_data 'u'
	do_lcd_data 't'
	do_lcd_data ' '
	do_lcd_data 'o'
	do_lcd_data 'f'
	do_lcd_data ' '
	do_lcd_data 'S'
	do_lcd_data 't'
	do_lcd_data 'o'
	do_lcd_data 'c'
	do_lcd_data 'k'

	do_lcd_command 0b11000000			; go to the next line

	do_lcd_bdata number					; display the number pressed

	clear_lcd_cursor					; clear the cursor

	ldi keyPressed, 1
	rjmp initKeypad

;===== COINS SCREEN =====
; the coins screen shows the number of coins to be inserted
; as well as the number of coins that have been inserted
coinsScreen:
	ldi currentScreen, coin				; set the current screen to "coin"

	loadImByte screenFlag, 1			; raise the screen flag

	do_lcd_command 0b00000001			; clear the lcd

	do_lcd_data 'I'
	do_lcd_data 'n'
	do_lcd_data 's'
	do_lcd_data 'e'
	do_lcd_data 'r'
	do_lcd_data 't'
	do_lcd_data ' '
	do_lcd_data 'c'
	do_lcd_data 'o'
	do_lcd_data 'i'
	do_lcd_data 'n'
	do_lcd_data 's'

	do_lcd_command 0b11000000			; go to the next line
	
	do_lcd_bdata remaining				; display the amount remaining

	clear_lcd_cursor					; clear the lcs cursor

	loadByte number, insertedCoins		; load the number of inserted coins into number
	rcall displayLEDnumber				; display the number of coins inserted on the LED 

	lds temp, remaining					; display the amount remaining
	cpi temp, 0
	breq deliveryScreen

	ldi keyPressed, 1					; if not, just register the key press
	rjmp initKeypad						; go back to scanning the keys

;===== DELIVERY SCREEN =====
; the delivery screen only appears once the user
; has put in enough coins for the product
deliveryScreen:
	ldi currentScreen, delivery			; set the current screen to "delivery"
	rcall deliverySuccess				; call the function which turns on the LEDs and motor

	loadImByte screenFlag, 1			; raise the screen flag

	do_lcd_command 0b00000001			; clear the lcd

	do_lcd_data 'D'
	do_lcd_data 'e'
	do_lcd_data 'l'
	do_lcd_data 'i'
	do_lcd_data 'v'
	do_lcd_data 'e'
	do_lcd_data 'r'
	do_lcd_data 'i'
	do_lcd_data 'n'
	do_lcd_data 'g'
	do_lcd_data ' '
	do_lcd_data 'I'
	do_lcd_data 't'
	do_lcd_data 'e'
	do_lcd_data 'm'

	do_lcd_command 0b00001100			; clear the cursor

	rjmp initKeypad

;===== ADMIN SCREEN =====
; the admin screen is only displayed if the * button has been pressed
; for five seconds while on the select screen
adminScreen:
	ldi currentScreen, adminMode		; set the current screen to "empty"

	loadImByte screenFlag, 1			; raise the screen flag
	
	do_lcd_command 0b00000001			; clear the lcd
	
	do_lcd_data 'A'
	do_lcd_data 'd'
	do_lcd_data 'm'
	do_lcd_data 'i'
	do_lcd_data 'n'
	do_lcd_data ' '
	do_lcd_data 'M'
	do_lcd_data 'o'
	do_lcd_data 'd'
	do_lcd_data 'e'
	do_lcd_data ' '
	do_lcd_bdigits adminScreenCount		; display the number pressed (or the default number)

	do_lcd_command 0b11000000			; go to the next line

	loadByte number, adminScreenCount	; set the adminNumber as number
	rcall getStock						; get the stock
	do_lcd_bdigits stockDisplay			; display the stock

	do_lcd_command 0b11001110			; set to decrement mode

	rcall getCost						; get the cost
	do_lcd_data '$'			
	do_lcd_bdigits costDisplay			; display the cost
	

	loadByte number, stockDisplay		; load the stock number to be displayed
	rcall displayLEDnumber				; display the amount on the LED

	ldi keyPressed, 1					; acknowledge key press
	rjmp initKeypad

;===== STARTUP SCREEN =====
; this is the first screen the users will see
startUpScreen:	
	ldi currentScreen, startUp			; set the current screen to start up

	loadImByte screenFlag, 1			; raise the screen flag

	do_lcd_command 0b00000001

	do_lcd_data '2'
	do_lcd_data '1'
	do_lcd_data '2'
	do_lcd_data '1'
	do_lcd_data ' '
	do_lcd_data '1'
	do_lcd_data '7'
	do_lcd_data 's'
	do_lcd_data '1'
	do_lcd_data ' '
	do_lcd_data ' '
	do_lcd_data ' '
	do_lcd_data 'O'
	do_lcd_data '1'
	do_lcd_data '0'
	
	do_lcd_command 0b11000000	; break to the next line

	do_lcd_data 'V'
	do_lcd_data 'e'
	do_lcd_data 'n'
	do_lcd_data 'd'
	do_lcd_data 'i'
	do_lcd_data 'n'
	do_lcd_data 'g'
	do_lcd_data ' '
	do_lcd_data 'M'
	do_lcd_data 'a'
	do_lcd_data 'c'
	do_lcd_data 'h'
	do_lcd_data 'i'
	do_lcd_data 'n'
	do_lcd_data 'e'

	clear_lcd_cursor			; clear the cursor

	rjmp initKeypad

; ===== DELIVERY FUNCTION =====
; called when a successful delivery is made
; turns on the motor and flashes the LED
; also decreases the number of the item
; received by the user
deliverySuccess:
	push temp

	rcall turnOnLED			; turn on LED to start the flash
	rcall turnOffPot		; turn off pot as it is no longer needed
	rcall turnOnMotor		; turn on motor to start the whirr

	clear insertedCoins		; clear the number of inserted coins
	clear remaining			; clear the number of coins remaining

	lds temp, requestedItem	; proceed to find and decrease the item received by the user
	cpi temp, 9
	equalTo delivered9
	cpi temp, 8
	equalTo delivered8
	cpi temp, 7
	equalTo delivered7
	cpi temp, 6
	equalTo delivered6
	cpi temp, 5
	equalTo delivered5
	cpi temp, 4
	equalTo delivered4
	cpi temp, 3
	equalTo delivered3
	cpi temp, 2
	equalTo delivered2

	delivered1:
		decrease item1		; decreases the item to a minimum of 0
		rjmp deliveryEnd
	delivered2:
		decrease item2
		rjmp deliveryEnd
	delivered3:
		decrease item3
		rjmp deliveryEnd
	delivered4:
		decrease item4
		rjmp deliveryEnd
	delivered5:
		decrease item5
		rjmp deliveryEnd
	delivered6:
		decrease item6
		rjmp deliveryEnd
	delivered7:
		decrease item7
		rjmp deliveryEnd
	delivered8:
		decrease item8
		rjmp deliveryEnd
	delivered9:
		decrease item9

	deliveryEnd:
		clear requestedItem	; clears the requested item

		pop temp
		ret
; ===== TURNING OFF THE POTENTIOMETER FUNCTION =====
; this function turns off the potentiometer when it is not in use
; disables the interrupt and the conversion
turnOffPot:
	push temp

	ldi temp1, (1<<MUX5) | (0<<ADSC) | (0<<ADIE)
	sts ADCSRB, temp1

	pop temp
	ret

; ===== TURNING ON THE POTENTIOMETER FUNCTION =====
; this function turns the potentiometer on to be used on the
; coins screen
; it enables the interrup and allows the conversion
turnOnPot:
	push temp

	ldi temp1, (1<<MUX5)| (1<<ADSC) | (1<<ADIE)
	sts ADCSRB, temp1

	pop temp
	ret

;===== TURNING ON THE LED FUNCTION =====
; this function turns on all 10 LEDs
turnOnLED:
	push temp

	lds temp, ledCount
	cpi temp, 3					; only turn it on 3 times!
	brne continueOn				; if already 3 times, leave it
	clear ledCount				; clear the count
	rjmp turnOnLEDend

	continueOn:
		increase ledCount
		ldi temp, 0b11111111	; turn on all the LEDs
		out PORTC, temp
		out PORTG, temp
		loadImByte LEDFlag, 1

	turnOnLEDend:
		pop temp
		ret

;===== TURNING OFF THE LED FUNCTION =====
; this function turns off the LEDs
turnOffLED:
	push temp

	clr temp
	out PORTC, temp
	out PORTG, temp
	clear LEDFlag

	pop temp
	ret

;===== TURNING ON THE MOTOR =====
; this function turns on the motor
turnOnMotor:
	push temp

	loadImByte PORTH, 0xFF
	loadImByte motorFlag, 1

	pop temp
	ret

;===== TURNING OFF THE MOTOR =====
; this function turns off the motor
turnOffMotor:
	push temp

	loadImByte PORTH, 0
	loadImByte motorFlag, 0

	pop temp
	ret

;===== LED DISPLAY FUNCTION =====
; this function displays the number on the LED from bottom up
displayLEDnumber:
	push temp
	push temp1
	lds temp, number
	cpi temp, 0
	breq show0
	cpi temp, 1
	breq show1
	cpi temp, 2
	breq show2
	cpi temp, 3
	breq show3
	cpi temp, 4
	breq show4
	cpi temp, 5
	breq show5
	cpi temp, 6
	breq show6
	cpi temp, 7
	breq show7
	cpi temp, 8
	breq show8
	cpi temp, 9
	breq show9

	ldi temp, 0b11111111
	out PORTC, temp
	out PORTG, temp
	rjmp displayLEDnumberEnd

	show0:
		clr temp
		out PORTC, temp
		out PORTG, temp
		rjmp displayLEDnumberEnd
	show1:
		out PORTC, temp
		clr temp
		out PORTG, temp
		rjmp displayLEDnumberEnd
	show2:
		ldi temp, 0b00000011
		out PORTC, temp
		clr temp
		out PORTG, temp
		rjmp displayLEDnumberEnd
	show3:
		ldi temp, 0b00000111
		out PORTC, temp
		clr temp
		out PORTG, temp
		rjmp displayLEDnumberEnd
	show4:
		ldi temp, 0b00001111
		out PORTC, temp
		clr temp
		out PORTG, temp
		rjmp displayLEDnumberEnd
	show5:
		ldi temp, 0b00011111
		out PORTC, temp
		clr temp
		out PORTG, temp
		rjmp displayLEDnumberEnd
	show6:
		ldi temp, 0b00111111
		out PORTC, temp
		clr temp
		out PORTG, temp
		rjmp displayLEDnumberEnd
	show7:
		ldi temp, 0b01111111
		out PORTC, temp
		clr temp
		out PORTG, temp
		rjmp displayLEDnumberEnd
	show8:
		ldi temp, 0b11111111
		out PORTC, temp
		clr temp
		out PORTG, temp
		rjmp displayLEDnumberEnd
	show9:
		ldi temp, 1
		out PORTG, temp
		rjmp displayLEDnumberEnd
	displayLEDnumberEnd:
		pop temp1
		pop temp
		ret

;===== GET STOCK FUNCTION ====
; this function retrieves the stock of a given item
getStock:
	push temp
	lds temp, number
	cpi temp, 9
	EqualTo getStock9
	cpi temp, 8
	EqualTo getStock8
	cpi temp, 7
	EqualTo getStock7
	cpi temp, 6
	EqualTo getStock6
	cpi temp, 5
	EqualTo getStock5
	cpi temp, 4
	EqualTo getStock4
	cpi temp, 3
	EqualTo getStock3
	cpi temp, 2
	EqualTo getStock2

	getStock1:
		loadByte stockDisplay, item1
		rjmp getStockEnd
	getStock2:
		loadByte stockDisplay, item2
		rjmp getStockEnd
	getStock3:
		loadByte stockDisplay, item3
		rjmp getStockEnd
	getStock4:
		loadByte stockDisplay, item4
		rjmp getStockEnd
	getStock5:
		loadByte stockDisplay, item5
		rjmp getStockEnd
	getStock6:
		loadByte stockDisplay, item6
		rjmp getStockEnd
	getStock7:
		loadByte stockDisplay, item7
		rjmp getStockEnd
	getStock8:
		loadByte stockDisplay, item8
		rjmp getStockEnd
	getStock9:
		loadByte stockDisplay, item9
	getStockEnd:
		pop temp
		ret

;===== GET COST FUNCTION =====
; this function retrieves the cost of a given item
getCost:
	push temp
	lds temp, number
	cpi temp, 9
	EqualTo getCost9
	cpi temp, 8
	EqualTo getCost8
	cpi temp, 7
	EqualTo getCost7
	cpi temp, 6
	EqualTo getCost6
	cpi temp, 5
	EqualTo getCost5
	cpi temp, 4
	EqualTo getCost4
	cpi temp, 3
	EqualTo getCost3
	cpi temp, 2
	EqualTo getCost2

	getCost1:
		loadByte costDisplay, cost1
		rjmp getCostEnd
	getCost2:
		loadByte costDisplay, cost2
		rjmp getCostEnd
	getCost3:
		loadByte costDisplay, cost3
		rjmp getCostEnd
	getCost4:
		loadByte costDisplay, cost4
		rjmp getCostEnd
	getCost5:
		loadByte costDisplay, cost5
		rjmp getCostEnd
	getCost6:
		loadByte costDisplay, cost6
		rjmp getCostEnd
	getCost7:
		loadByte costDisplay, cost7
		rjmp getCostEnd
	getCost8:
		loadByte costDisplay, cost8
		rjmp getCostEnd
	getCost9:
		loadByte costDisplay, cost9
	getCostEnd:
		pop temp
		ret

;===== CONVERT DIGITS FUNCTION =====
; this function converts a number in a register into something that can be displayed
; as more than one digit
convertDigits:
	push temp

	clear hundredsDigit				; clear the data to be used
	clear tensDigit

	lds temp, toConvert				; retrieve the number to be converted
	cpi temp, 100					; check if the number is equal to or greater than
	brsh hundreds					; 100
	cpi temp, 10					; check if the number is equal to or greater than 10
	brsh tens			
	rjmp convertDigitsEnd			; if only one digit, just go to the end

	hundreds:
		increase hundredsDigit		; keep decreasing until there are no more 100s left	
		subi temp, 100		
		cpi temp, 100		
		brsh hundreds
		do_lcd_bdata hundredsDigit	; display the number of 100s
		cpi temp, 10				; check to see if the new number is greater than 10
		brsh tens			
		do_lcd_data '0'				; if not, just display 0 and go to the end to display		
		rjmp convertDigitsEnd		; the last digit

	tens:
		increase tensDigit			; keep decreasing until there are no more 10s left
		subi temp, 10
		cpi temp, 10
		brsh tens
		do_lcd_bdata tensDigit		; display the number of 10s

	convertDigitsEnd:
		do_lcd_rdata temp			; display what's left (the last digit)

		pop temp
		ret

; Send a command to the LCD (lcd register)

lcd_command:
	out PORTF, lcd
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	lcd_clr LCD_E
	rcall sleep_1ms
	ret

lcd_data:
	push temp
	out PORTF, lcd
	lcd_set LCD_RS
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	lcd_clr LCD_E
	rcall sleep_1ms
	lcd_clr LCD_RS
	pop temp
	ret

lcd_wait:
	push lcd
	push temp
	clr lcd
	out DDRF, lcd
	out PORTF, lcd
	lcd_set LCD_RW

lcd_wait_loop:
	rcall sleep_1ms
	lcd_set LCD_E
	rcall sleep_1ms
	in lcd, PINF
	lcd_clr LCD_E
	sbrc lcd, 7
	rjmp lcd_wait_loop
	lcd_clr LCD_RW
	ser lcd
	out DDRF, lcd
	pop lcd
	pop temp
	ret

lcd_second_end:
	push temp
	do_lcd_command 0b11001111
	do_lcd_command 0b00000100
	pop temp
	ret

.equ F_CPU = 16000000
.equ DELAY_1MS = F_CPU / 4 / 1000 - 4
; 4 cycles per iteration - setup/call-return overhead

sleep_1ms:
	push r24
	push r25
	ldi r25, high(DELAY_1MS)
	ldi r24, low(DELAY_1MS)
delayloop_1ms:
	sbiw r25:r24, 1
	brne delayloop_1ms
	pop r25
	pop r24
	ret

sleep_5ms:
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	rcall sleep_1ms
	ret